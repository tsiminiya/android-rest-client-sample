package com.ccti.sample.rest.web.util;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {

	public static JSONObject parseJsonObject(final String jsonString) throws JSONException {
		return new JSONObject(jsonString);
	}
	
}
