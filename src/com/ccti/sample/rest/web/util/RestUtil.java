package com.ccti.sample.rest.web.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.net.Uri;
import android.net.Uri.Builder;

public class RestUtil {

	public static String doGet(final String uri, final Map<String, String> paramMap) throws Exception {
		HttpClient httpClient = new DefaultHttpClient();
		Builder uriBuilder = Uri.parse(uri).buildUpon();
		// Set Http Parameters
		if (paramMap != null) {
			for (String key : paramMap.keySet()) {
				uriBuilder.appendQueryParameter(key, paramMap.get(key));
			}
		}
		HttpGet httpGet = new HttpGet(uriBuilder.toString());
		HttpEntity responseEntity = null;
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			// Store contents in an outputstream
			responseEntity = httpResponse.getEntity();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			responseEntity.writeTo(byteArrayOutputStream);
			return byteArrayOutputStream.toString();
		} catch (ClientProtocolException e) {
			throw new Exception("Error in client protocol", e);
		} catch (IOException e) {
			throw new Exception("I/O Error", e);
		} finally {
			if (responseEntity != null) {
				responseEntity.consumeContent();
			}
			httpClient.getConnectionManager().shutdown();
		}
	}
	
	public static String doPost(final String uri, final Map<String, String> paramMap) throws Exception {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(uri);
		// Set Http Parameters
		if (paramMap != null) {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); 
			for (String key : paramMap.keySet()) {
				nameValuePairs.add(new BasicNameValuePair(key, paramMap.get(key)));
			}
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairs);
			httpPost.setEntity(urlEncodedFormEntity);
		}
		HttpEntity responseEntity = null;
		try {
			HttpResponse httpResponse = httpClient.execute(httpPost);
			// Store contents in an outputstream
			responseEntity = httpResponse.getEntity();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			responseEntity.writeTo(byteArrayOutputStream);
			return byteArrayOutputStream.toString();
		} catch (ClientProtocolException e) {
			throw new Exception("Error in client protocol", e);
		} catch (IOException e) {
			throw new Exception("I/O Error", e);
		} finally {
			if (responseEntity != null) {
				responseEntity.consumeContent();
			}
			httpClient.getConnectionManager().shutdown();
		}
	}
	
}
