package com.ccti.rest.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ccti.sample.rest.web.util.JsonUtil;
import com.ccti.sample.rest.web.util.RestUtil;

public class SignUpUserActivity extends Activity {

	private static final String TAG = "SignUpUserActivity";
	private static final int FIELD_LEFT_BLANK = -1;
	private static final int USER_ALREADY_EXIST = -2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Sign-up");
		setContentView(R.layout.sign_up);
	}

	public void signUp(View view) {
		final EditText usernameEditTextView = (EditText) findViewById(R.id.usernameEditTextView);
		final EditText passwordEditTextView = (EditText) findViewById(R.id.passwordEditTextView);
		if (usernameEditTextView != null && passwordEditTextView != null) {
			SignUpUserTask signUpUserTask = new SignUpUserTask();
			signUpUserTask.execute(usernameEditTextView.getText().toString(), passwordEditTextView.getText().toString());
		}
	}
	
	private class SignUpUserTask extends AsyncTask<String, Integer, String> {
		
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(SignUpUserActivity.this, "Create User", "Creating user...");
			progressDialog.show();
		}
		
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			if (result == null || result.isEmpty()) {
				Toast.makeText(SignUpUserActivity.this, "Unable to create user", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(SignUpUserActivity.this, "Created user", Toast.LENGTH_LONG).show();
			}
			finish();
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			if (values[0] == FIELD_LEFT_BLANK) {
				Toast.makeText(SignUpUserActivity.this, "Please don't leave fields blank", Toast.LENGTH_LONG).show();
			} else if (values[0] == USER_ALREADY_EXIST) {
				Toast.makeText(SignUpUserActivity.this, "User already exist!", Toast.LENGTH_LONG).show();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			if (params.length > 1) {
				String name = params[0];
				String password = params[1];
				if (name == null || name.isEmpty() || password == null || password.isEmpty()) {
					publishProgress(FIELD_LEFT_BLANK);
				} else {
					boolean userAvailable = isUsernameAvailable(name);
					if (userAvailable) {
						String id = signUpUser(name, password);
						return id;
					} else {
						publishProgress(USER_ALREADY_EXIST);
					}
				}
			} else {
				publishProgress(FIELD_LEFT_BLANK);
			}
			return null;
		}
		
		private String signUpUser(final String username, final String password) {
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("name", username);
			paramMap.put("passwd", password);
			String uri = "http://" + getResources().getString(R.string.host_name) + ":8888/rest/web/resource/user/create";
			try {
				String resource = RestUtil.doPost(uri, paramMap);
				return JsonUtil.parseJsonObject(resource).getString("user-id");
			} catch (NotFoundException e) {
				Log.e(TAG, "Unable to find URI, " + uri, e);
			} catch (JSONException e) {
				Log.e(TAG, "JSON parsing exception", e);
			} catch (Exception e) {
				Log.e(TAG, "Error in retrieving web resource", e);
			}
			return null;
		}
		
		private Boolean isUsernameAvailable(final String username) {
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("name", username);
			String uri = "http://" + getResources().getString(R.string.host_name) + ":8888/rest/web/resource/user/available";
			try {
				String resource = RestUtil.doGet(uri, paramMap);
				return JsonUtil.parseJsonObject(resource).getBoolean("available");
			} catch (NotFoundException e) {
				Log.e(TAG, "Unable to find URI, " + uri, e);
			} catch (JSONException e) {
				Log.e(TAG, "JSON parsing exception", e);
			} catch (Exception e) {
				Log.e(TAG, "Error in retrieving web resource", e);
			}
			return null;
		}
		
	}
	
}
