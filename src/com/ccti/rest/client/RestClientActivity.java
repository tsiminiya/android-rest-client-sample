package com.ccti.rest.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class RestClientActivity extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_list);
        final ListView listView = (ListView) findViewById(R.id.mainMenuListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				switch (position) {
					case 0:
						startActivity(new Intent(RestClientActivity.this, SignUpUserActivity.class));
						break;
					case 1:
						startActivity(new Intent(RestClientActivity.this, LogInUserActivity.class));
						break;
					default:
				}
			}
			
		});
    }
    
}