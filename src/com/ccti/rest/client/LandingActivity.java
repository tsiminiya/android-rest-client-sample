package com.ccti.rest.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import com.ccti.sample.rest.web.util.JsonUtil;
import com.ccti.sample.rest.web.util.RestUtil;

import android.app.Activity;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class LandingActivity extends Activity {

	private static final String TAG = "LandingActivity";
	
	private String username;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.landing);
	    TextView usernameTextView = (TextView) findViewById(R.id.username);
	    username = getIntent().getStringExtra("username");
	    usernameTextView.setText(username);
	}

	public void logout(final View view) {
		LogoutUserTask logoutUserTask = new LogoutUserTask();
		logoutUserTask.execute(username);
	}
	
	private class LogoutUserTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected void onPostExecute(Boolean result) {
			if (result == null) {
				Toast.makeText(LandingActivity.this, "Error in logging-out user", Toast.LENGTH_LONG).show();
			} else if (result) {
				Toast.makeText(LandingActivity.this, "User logged-out", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(LandingActivity.this, "Unable to log-out user", Toast.LENGTH_LONG).show();
			}
			finish();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			if (params.length > 0) {
				String name = params[0];
				return logoutUser(name);
			}
			return null;
		}
		
		private Boolean logoutUser(final String username) {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("name", username);
			String uri = "http://" + getResources().getString(R.string.host_name) + ":8888/rest/web/resource/user/logout";
			try {
				String resource = RestUtil.doGet(uri, paramsMap);
				return JsonUtil.parseJsonObject(resource).getBoolean("logged-out");
			} catch (NotFoundException e) {
				Log.e(TAG, "Unable to find URI, " + uri, e);
			} catch (JSONException e) {
				Log.e(TAG, "JSON parsing exception", e);
			} catch (Exception e) {
				Log.e(TAG, "Error in retrieving web resource", e);
			}
			return null;
		}
		
	}
	
}
