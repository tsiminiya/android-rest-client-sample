package com.ccti.rest.client;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;

import com.ccti.sample.rest.web.util.JsonUtil;
import com.ccti.sample.rest.web.util.RestUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LogInUserActivity extends Activity {

	private static final String TAG = "LogInUserActivity";
	private static final int FIELD_LEFT_BLANK = -1;
	private static final int INVALID_LOG_IN = 2;
	private static final int LOG_IN_SUCCESSFUL = 1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.log_in);
	}
	
	public void login(final View view) {
		final EditText usernameEditTextView = (EditText) findViewById(R.id.usernameEditTextView);
		final EditText passwordEditTextView = (EditText) findViewById(R.id.passwordEditTextView);
		if (usernameEditTextView != null && passwordEditTextView != null) {
			LoginUserTask loginUserTask = new LoginUserTask();
			try {
				Integer loginResult = loginUserTask.execute(usernameEditTextView.getText().toString(), passwordEditTextView.getText().toString()).get();
				if (loginResult == LOG_IN_SUCCESSFUL) {
					Intent landingActivityIntent = new Intent(LogInUserActivity.this, LandingActivity.class);
					landingActivityIntent.putExtra("username", usernameEditTextView.getText().toString());
					startActivity(landingActivityIntent);
				}
			} catch (InterruptedException e) {
				Log.e(TAG, "Thread interrupted", e);
			} catch (ExecutionException e) {
				Log.e(TAG, "Task execution error", e);
			}
		}
	}
	
	private class LoginUserTask extends AsyncTask<String, Integer, Integer> {

		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(LogInUserActivity.this, "Log-in", "Logging-in...");
			progressDialog.show();
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			if (values[0] == FIELD_LEFT_BLANK) {
				Toast.makeText(LogInUserActivity.this, "Please don't leave fields blank", Toast.LENGTH_LONG).show();
			}
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			progressDialog.dismiss();
			if (result == INVALID_LOG_IN) {
				Toast.makeText(LogInUserActivity.this, "Invalid Log-in!", Toast.LENGTH_LONG).show();
			} else if (result == LOG_IN_SUCCESSFUL) {
				Toast.makeText(LogInUserActivity.this, "Log-in successful!", Toast.LENGTH_LONG).show();
			}
			finish();
		}
		
		@Override
		protected Integer doInBackground(String... params) {
			if (params.length > 1) {
				String name = params[0];
				String password = params[1];
				return loginUser(name, password);
			} else {
				publishProgress(FIELD_LEFT_BLANK);
			}
			return null;
		}
		
		private Integer loginUser(final String username, final String password) {
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("name", username);
			paramMap.put("passwd", password);
			String uri = "http://" + getResources().getString(R.string.host_name) + ":8888/rest/web/resource/user/login";
			try {
				String resource = RestUtil.doPost(uri, paramMap);
				return JsonUtil.parseJsonObject(resource).getInt("login-result");
			} catch (NotFoundException e) {
				Log.e(TAG, "Unable to find URI, " + uri, e);
			} catch (JSONException e) {
				Log.e(TAG, "JSON parsing exception", e);
			} catch (Exception e) {
				Log.e(TAG, "Error in retrieving web resource", e);
			}
			return null;
		}
		
	}

}
